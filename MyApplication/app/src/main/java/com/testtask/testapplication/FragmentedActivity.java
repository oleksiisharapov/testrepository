package com.testtask.testapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FragmentedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmented);
    }
}
